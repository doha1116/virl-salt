{% set ifproxy = salt['grains.get']('proxy', 'False') %}
{% set masterless = salt['pillar.get']('virl:salt_masterless', salt['grains.get']('salt_masterless', false)) %}

##Include openstack.repo and common.pip sls file when running
include:
  - openstack.repo
  - common.pip

##Manage unix group setting - virl
virl-group:
  group.present:
    - name: virl

##Manage unix group setting- libvirtd
libvirt-group:
  group.present:
    - name: libvirtd

##Create and manage user settings. Set virl-user to present and set name and password.
##User's full name is virl. The login shell is at /bin/bash and home is at /home/virl.
##Password set to the crypt below.
virl-user:
  user.present:
    - name: virl
    - fullname: virl
    - name: virl
    - shell: /bin/bash
    - home: /home/virl
    - password: $6$SALTsalt$789PO2/UvvqTk1tGEj67KEOSPbQqqd9wEEBPqTrAuqNO1rTeNruN.IiVxXZX6w8kfEnt7q5eyz/aOFwlZow/b0

##Manipute file on the system. Set /etc/sudoers.d/virl to mode 0440.
##Set create default to True. If false, then file only managed if the file already existent.
/etc/sudoers.d/virl:
  file.managed:
    - order: 3
    - mode: 0440
    - create: True

##Ensure that text shown below appear at end of file at location of /etc/sudoers.d/virl. 
##Require user virl-user to be present
sudoer-defaults:
    file.append:
        - order: 4
        - name: /etc/sudoers.d/virl
        - require:
          - user: virl-user
        - text:
          - virl ALL=(root) NOPASSWD:ALL
          - Defaults:virl secure_path=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin:/opt:/opt/bin:/opt/support
          - Defaults env_keep += "http_proxy https_proxy HTTP_PROXY HTTPS_PROXY OS_TENANT_NAME OS_USERNAME OS_PASSWORD OS_AUTH_URL"

##Set up openssh-server to be installed. Set refresh to false.
openssh-server:
  pkg.installed:
   - refresh: False

##Set up crudini to be installed. Set refresh to false.
crudini:
  pkg.installed:
   - refresh: False

{% for pyreq in 'wheel','envoy','docopt','sh','configparser>=3.3.0r2' %}
{{ pyreq }}:
  pip.installed:
    - require:
      - file: first-vinstall
    {% if ifproxy == True %}
    {% set proxy = salt['grains.get']('http proxy', 'None') %}
    - proxy: {{ proxy }}
    {% endif %}
{% endfor %}

##Create symlink of target at /usr/bin/crudini and require crudini pkg.
/usr/local/bin/openstack-config:
  file.symlink:
    - target: /usr/bin/crudini
    - mode: 0755
    - require:
      - pkg: crudini

##If not masterless, download vinstall.py and run it for user virl. Else, create symlink with
##target from /sr/salt/virl/files/vininstall.py.
##Set perm mode to 0755 and user/group to virl.
first-vinstall:
{% if not masterless %}
  file.managed:
    - name: /usr/local/bin/vinstall
    - source: salt://virl/files/vinstall.py
    - user: virl
    - group: virl
    - mode: 0755
{% else %}
  file.symlink:
    - name: /usr/local/bin/vinstall
    - target: /srv/salt/virl/files/vinstall.py
    - mode: 0755
    - force: true
    - onlyif: 'test -e /srv/salt/virl/files/vinstall.py'
{% endif %}
