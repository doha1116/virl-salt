##Verifies that the vagrant SSH key is present for the user
##SSH key to manage is vagrant nad user who owns it is virl.
##Type of key is ssh-rsa and download authorized keys from source.
vagrant open key:
  ssh_auth.present:
    - name: vagrant
    - user: virl
    - enc: ssh-rsa
    - source: 'salt://files/authorized_keys'