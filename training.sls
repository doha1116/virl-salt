##Salt states for training materials

##This directory on the salt master to be recursively copied down to the minion.
##Also run service apache2 restart after /var/www/training.
##File mode and directory mode set to 755. Wait until apache2 restart has finished
##And watch for file to be completed.
/var/www/training:
  file.recurse:
    - file_mode: 755
    - dir_mode: 755
    - makedirs: True
    - source: "salt://files/training"
  cmd.wait:
    - name: service apache2 restart
    - watch:
      - file: /var/www/training

##This directory on the salt master to be recursively copied down to the minion.
##Also run service apache2 restart after /var/www/training.
##File mode and directory mode set to 755. Wait until apache2 restart has finished
##And watch for file to be completed.
/var/www/doc:
  file.recurse:
    - file_mode: 755
    - dir_mode: 755
    - makedirs: True
    - source: "salt://files/virl.standalone/glocal/std/virl-cli/doc/build/html"
  cmd.wait:
    - name: service apache2 restart
    - watch:
      - file: /var/www/doc
