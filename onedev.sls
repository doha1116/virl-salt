##Unsure if this file is needed

##Recurse through a subdirectory on the master and copy subdirectory over to the specified path.
##Set the recursion in /var/cache/virl/devops and source listed at "salt://images/devops-setup"
##Run the command ./devops_installer.sh -default -a in directory devops after devops copy completes.
devops copy:
  file.recurse:
    - name: /var/cache/virl/devops
    - file_mode: 755
    - dir_mode: 755
    - source: "salt://images/devops-setup"
  cmd.wait:
      - cwd: /var/cache/virl/devops
      - name: ./devops_installer.sh -default -a
      - watch:
        - file: devops copy
