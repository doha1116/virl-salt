##Salt state to remove openssh-server and reapply
##Shouldn't this be included somewhere in virl directory?
##Even if it's not, unsure if this file is needed

##Remove openssh-server
##Skip the GPG verification check for the package to be installed
##Set updating the repo database of available packages prior to installing false
cleaning ssh off:
  pkg.removed:
    - order: 1
    - skip_verify: True
    - refresh: False
    - pkgs:
      - openssh-server

##Install openssh-server
##Skip the GPG verification check for the package to be installed
##Set updating the repo database of available packages prior to installing false
reapplying ssh:
  pkg.installed:
    - order: 2
    - skip_verify: True
    - refresh: False
    - pkgs:
      - openssh-server
